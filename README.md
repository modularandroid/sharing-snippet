# Sharing Snippet

The purpose of this documentation is to provide an overview about the typical use cases of the Android  Sharing intent.



## Use Case 1 : Sharing HTML

Let's say that in your application, you have an activity that contains a button ***sharingButton***. You want to share HTML text when a click is made on this button.

```java
Button sharingButton = findViewById(R.id.sharingButton);
sharingButton.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        
        //Intent for sharing HTML
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/html");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, Html.fromHtml("<p>This is the text shared.</p>"));
        startActivity(Intent.createChooser(sharingIntent, "Share HTML"));
        
	}
});
```



## Use Case 2 : Sharing Image

Let's say that in your application, you have an activity that contains a button ***sharingButton***. You want to share a local Image when a click is made on this button.

```java
Button sharingButton = findViewById(R.id.sharingButton);
sharingButton.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        
        //Creating Image File
        final File imageFile = new File("YOUR_IMAGE_PATH");
        
        //Intent for sharing Image
		final Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("image/*");
        shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(imageFile));
        startActivity(Intent.createChooser(shareIntent, "Share Image"));
        
	}
});
```

*For this use case, you must make sure that the image has the necessary rights to be exploited by another application. In general, images stored in the path returned by the getFilesDir() method have the necessary rights.*

*[Gmail case] Since Gmail 5.0, security checks are stricter. Therefore, if you want to share an image with Gmail, you need it to be stored in accordance with the directories mentioned [here](https://stackoverflow.com/questions/26883259/gmail-5-0-app-fails-with-permission-denied-for-the-attachment-when-it-receives?answertab=active#tab-top) .* 
*Another solution is to use a content provider (discussed below in this snippet).*



## Use Case 3 : Sharing Multiple Images

Let's say that in your application, you have an activity that contains a button ***sharingButton***. You want to share local Images when a click is made on this button.

```java
Button sharingButton = findViewById(R.id.sharingButton);
sharingButton.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        
        //Creating Image Files
        final File imageFile1 = new File("YOUR_IMAGE_PATH_1");
        final File imageFile2 = new File("YOUR_IMAGE_PATH_2");
        final File imageFile3 = new File("YOUR_IMAGE_PATH_3");
        
        //Creating arrayList of URIs with these imageFiles
        ArrayList<Uri> imageUris = new ArrayList<>();
        imageUris.add(Uri.fromFile(imageFile1));
        imageUris.add(Uri.fromFile(imageFile2));
        imageUris.add(Uri.fromFile(imageFile3));
        
        //Intent for sharing Images
        Intent shareIntent = new Intent(Intent.ACTION_SEND_MULTIPLE);
        shareIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, imageUris);
        shareIntent.setType("image/*");
        
	}
});
```



## Use Case 4 : Sharing Link

Let's say that in your application, you have an activity that contains a button ***sharingButton***. You want to share a link when a click is made on this button.

```java
Button sharingButton = findViewById(R.id.sharingButton);
sharingButton.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        
        //Intent for sharing a link
		Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain"); 
        shareIntent.putExtra(Intent.EXTRA_TEXT, "http://codepath.com");
        startActivity(Intent.createChooser(shareIntent, "Share link"));
        
	}
});
```



## Use Case 5 : Sharing Multiple Types

Let's say that in your application, you have an activity that contains a button ***sharingButton***. You want to share a text and an image when a click is made on this button.

```java
Button sharingButton = findViewById(R.id.sharingButton);
sharingButton.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        
        //Creating text and image to share
		String text = "Look at my awesome picture";
        final File imageFile = new File("YOUR_IMAGE_PATH");
        
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_TEXT, text);
        shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(imageFile));
        shareIntent.setType("image/*");
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        
        startActivity(Intent.createChooser(shareIntent, "Share text and image"));
        
	}
});
```

*Note that Facebook does not properly recognize multiple shared elements. See [this facebook specific bug for more details](https://developers.facebook.com/x/bugs/332619626816423/) and share using their SDK.* 



## Use Case 6 : Sharing on Facebook

Facebook doesn't work well with normal sharing intents when sharing multiple content elements as [discussed in this bug](https://developers.facebook.com/x/bugs/332619626816423/). To share posts with facebook, we need to:

1. Create a [new Facebook app here](https://developers.facebook.com/apps/) (follow the instructions)
2. Add the Facebook SDK to your Android project
3. Share using this code snippet:

```java
   public void setupFacebookShareIntent() {
        ShareDialog shareDialog;
        FacebookSdk.sdkInitialize(getApplicationContext());
        shareDialog = new ShareDialog(this);

        ShareLinkContent linkContent = new ShareLinkContent.Builder()
                .setContentTitle("Title")
                .setContentDescription("\"Body Of Test Post\"")
                .setContentUrl(Uri.parse("http://someurl.com/here"))
                .build();
        
        shareDialog.show(linkContent);
    }
```



## Additional : Sharing with API 24 or higher

If you are targeting Android API 24 or higher, private URI resources (file:///) cannot be shared.  You must instead wrap the file object as a content provider (content://) using the [FileProvider](https://developer.android.com/reference/android/support/v4/content/FileProvider.html) class.

First, you must declare this FileProvider in your `AndroidManifest.xml` file within the `<application>` tag:

```xml
<application>

    <provider
              android:name="android.support.v4.content.FileProvider"
              android:authorities="com.codepath.fileprovider"
              android:exported="false"
              android:grantUriPermissions="true">
        <meta-data
                   android:name="android.support.FILE_PROVIDER_PATHS"
                   android:resource="@xml/fileprovider" />
    </provider>
    
</application>
```



Next, create a resource directory called `xml` and create a `fileprovider.xml`.  Assuming you wish to grant access to the application's specific external storage directory, which requires requesting no additional permissions, you can declare this line as follows:

```xml
<?xml version="1.0" encoding="utf-8"?>
<paths>
    <external-files-path
        name="images"
        path="Pictures" />
</paths>
```



Finally, you will convert the File object into a content provider using the FileProvider class:

```java
// getExternalFilesDir() + "/Pictures" should match the declaration in fileprovider.xml paths
File file = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), "YOUR_FILE_NAME");

//Wrap file object into a content provider. NOTE: authority here should match authority in manifest declaration
bmpUri = FileProvider.getUriForFile(YOUR_ACTIVITY_NAME.this, "YOUR_PACKAGE_NAME", file);
```



Note that there are other XML tags you can use in the `fileprovider.xml`, which map to the File directory specified.  In the example above, we use `Context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)`, which corresponded to the `<external-files-dir>` XML tag in the declaration with the `Pictures` path explicitly specified.  

Here are all the options you can use too :

| XML tag                   | Corresponding storage call                | When to use      |
|---------------------------|-------------------------------------------|------------------|
| &lt;files-path>           | Context.getFilesDir()                     | data can only be viewed by app, deleted when uninstalled (`/data/data/[packagename]/files`) |
| &lt;external-files-dir>   | Context.getExternalFilesDir()             | data can be read/write by the app, any apps granted with READ_STORAGE permission can read too, deleted when uninstalled (`/Android/data/[packagename]/files`) |
| &lt;cache-path>           | Context.getCacheDir()                     | temporary file storage |
| &lt;external-path>        | Environment.getExternalStoragePublicDirectory() | data can be read/write by the app, any apps can view, files not deleted when uninstalled  |
| &lt;external-cache-path>  | Context.getExternalCacheDir()             | temporary file storage with usually larger space |



*Note that this snippet was largely inspired by this [wiki](https://github.com/codepath/android_guides/wiki/Sharing-Content-with-Intents) . If you want more detailed use cases, don't hesitate to have a look at it.*